import { Iterable, fromJS } from 'immutable';

let instance = null; // make it a singleton

class SimpleState {
    constructor() {
        if (!instance) {
            instance = this;

            this.listeners = [];
        }

        return instance;
    }

    // determines if simple state has a listener with the supplied id
    hasListener(id) {
        return (this.indexOf(id) >= 0);
    }

    // gets the listener index with the supplied id
    indexOf(id) {
        for (let i=0; i<this.listeners.length; i++) {
            let listener = this.listeners[i];
            if (listener.id == id) {
                return i;
            }
        }

        return -1;
    }

    // adds a listener with the supplied id iff it doesn't already exist
    addListener(id, state={}) {
        if (this.hasListener(id)) throw new Error('Unable to add duplicate listener id: ' + id);
        
        let listener = {
            id: id,
            state: fromJS(state),
            subscriptions: []
        };

        this.listeners.push(listener);
    }

    // removes a listener with the supplied id iff it exists
    removeListener(id) {
        let index = this.indexOf(id);

        if (index >= 0) {
            this.listeners.splice(index, 1);
            return;
        }

        throw new Error('Unable to remove listener with supplied id: ' + id);
    }

    // subscribes to a listener with the supplied id iff it exists
    subscribe(id, component, subscription) {
        let index = this.indexOf(id);

        if (index >= 0) {
            let listener = this.listeners[index];

            listener.subscriptions.push({
                component: component,
                subscription: subscription
            });

            return listener.state;
        }

        throw new Error('Unable to subscribe to listener with supplied id: ' + id);
    }

    // unsubscribes component with the suplied id iff it exists and the component has a subscription
    unsubscribe(id, component) {
        let index = this.indexOf(id);

        if (index >= 0) {
            let listener = this.listeners[index];

            for (let i = 0; i < listener.subscriptions.length; i++) {
                let subscription = listener.subscriptions[i];
                if (subscription.component == component) {
                    listener.subscriptions.splice(i, 1);
                    return;
                }
            }
            
            throw new Error('Unable to unsubscribe from listener with unsubscribed component');
        }

        throw new Error('Unable to unsubscribe from listener with supplied id: ' + id);
    }

    // evokes all subscriptions to a listener with the supplied id iff it exists, updates the state of the listener
    evoke(id, state={}) {
        let index = this.indexOf(id);

        if (index >= 0) {
            let listener = this.listeners[index];
            listener.state = fromJS(state);

            let nextState = this.iterableObject(listener.state);

            for (let i=0; i<listener.subscriptions.length; i++) {
                listener.subscriptions[i].subscription(nextState);
            }

            return nextState;
        }

        throw new Error('Unable to evoke subscription updates for listener with supplied id: ' + id);
    }

    // gets the current state of a listener with supplied id iff it exists
    getState(id) {
        let index = this.indexOf(id);

        if (index >= 0) return this.iterableObject(this.listeners[index].state);

        throw new Error('Unable to get state from listener with supplied id: ' + id);
    }

    // converts an immutable iterable object to a plain js object if it is an iterable. else returns the object passed in
    iterableObject(obj={}) {
        return (Iterable.isIterable(obj) ? obj.toJS() : obj);
    }
}

module.exports = SimpleState;