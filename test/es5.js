var SimpleState = require('../');

var simpleState = new SimpleState();

simpleState.addListener('firstName', 'bob');

simpleState.subscribe('firstName', 'dummySub', function(nextState) {
    console.log('next state in dummySub: ', nextState);
});

simpleState.evoke('firstName', 'vance');

simpleState.evoke('firstName', {name: 'vance'});

simpleState.unsubscribe('firstName', 'dummySub');

simpleState.evoke('firstName', 'smith');